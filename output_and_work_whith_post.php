<?php
$mysql = mysqli_connect("localhost","root","","to20v2");

    
class Post{
    private $caption="";
    private $text="";
    private $user_id=0;
    
    function __construct($caption,$text,$user_id){          
        
        $this->caption = $caption;
        $this->text = $text;
        $this->user_id = $user_id;        
    }
    function get_caption(){
        return $this->caption ;
    }
    function get_text(){
        return $this->text ;
    }
    function get_user_id(){
        return $this->user_id;
    }  
    function set($new_caption,$new_text,$new_user_id){
        
        $this->caption=$new_caption;
        $this->text=$new_text;
        $this->user_id=$new_user_id;
        
        
    }
}

function insertPostToDb( $db, $post )
{
    $caption = $post->get_caption();
    $text = $post->get_text();
    $user_id = $post->get_user_id();

    $sql="  INSERT INTO `post`(`caption`, `text`, `user_id`)
            VALUES ( '$caption', '$text', $user_id )";

    mysqli_query($db, $sql);
}


if( $_POST['button'] != NULL )
{
    $post = new Post( $_POST['caption'], $_POST['text'], 123 );
    insertPostToDb($mysql,$post);//ERROR!!!!!!!!!!!!!
}



//============================================================
//
//                  Вывод постов
//
//============================================================
function select_posts($mysql){
    $query = "SELECT * FROM post ORDER BY id DESC ";
    //сохраняем всё в result чтобы получить выбранные данные из БД
    $result = mysqli_query($mysql,$query);
    $array=[];
    for ($i = 0; $i < $result->num_rows; $i++){
        $row = $result->fetch_assoc();
        $array[] = new Post($row['text'], $row['caption'], 0);
    }
    return $array;  
}

//чтобы достать записи из переменной $result - используем fetch_assoc
//fetch_assoc - вернет нам ассоциативный массив с выборкой ОДНОЙ строки из БД
//он будет выбирать их с самого начала. После того, как он вернул запись
//он автоматически перейдет на след. запись
$a=select_posts($mysql);
for ($i=0;$i<count($a);$i++){
    require "post.php";
}

mysqli_close($mysql);
?>
